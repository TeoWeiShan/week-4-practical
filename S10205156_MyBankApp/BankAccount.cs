﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S10205156_MyBankApp
{
    class BankAccount
    {
        private string accNo;

        public string AccNo
        {
            get { return accNo; }
            set { accNo = value; }
        }

        private string accName;

        public string AccName
        {
            get { return accName; }
            set { accName = value; }
        }

        private double balance;

        public double Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        public BankAccount()
        {

        }

        public BankAccount (string accNo, string accName, double balance)
        {
            AccNo = accNo;
            AccName = accName;
            Balance = balance;
        }

        public void Deposit(double amount)
        {
            Balance += amount;
        }

        public bool Withdraw(double amount)
        {
            if (amount <= Balance)
            {
                Balance -= amount;
                return true;
            }
            return false;
        }

        public override string ToString()
        {

            return "Account No : " + AccNo + " Account Name: " + AccName + " Acc Balance: " + Balance;
        }
    }
}
