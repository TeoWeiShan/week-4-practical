﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S10205156_MyBankApp
{
    class SavingsAccount: BankAccount
    {
        private double rate;

        public double Rate
        {
            get { return rate; }
            set { rate = value; }
        }

        public SavingsAccount() : base()
        {

        }

        public SavingsAccount(string accNo, string accName, double balance, double rate): base(accNo,accName,balance) 
        {
            AccNo = accNo;
            AccName = accName;
            Balance = balance;
            Rate = rate;
        }

        public double CalculateInterest(double amount)
        {
            double interestAmt = Balance * amount/100;
            return interestAmt;

        }

        /*public double CalcInt()
        {
            return Balance * Rate / 100;
        }*/

        public override string ToString()
        {
            return base.ToString() + " Rate: " + Rate;
        }


    }
}
