﻿using System;
using System.Collections.Generic;
using System.IO;

namespace S10205156_MyBankApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] csvLines = File.ReadAllLines("savings_account(2).csv");
            List<SavingsAccount> savingsAccList = new List<SavingsAccount>() { };
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] column = csvLines[i].Split(',');
                SavingsAccount number = new SavingsAccount(column[0], column[1], Convert.ToDouble(column[2]), Convert.ToDouble(column[3]));
                savingsAccList.Add(number);
            }

            while (true)
            {

                Console.WriteLine("\nMenu");
                Console.WriteLine("[1] Display all accounts");
                Console.WriteLine("[2] Deposit");
                Console.WriteLine("[3] Withdraw");
                Console.WriteLine("[4] Display details");
                Console.WriteLine("[0] Exit");

                Console.Write("Enter your option : ");
                string option = Console.ReadLine();
                Console.Write("\n");


                if (option == "1")
                    DisplayAll(savingsAccList);
                else if (option == "2")
                    DepositOption(savingsAccList);
                else if (option == "3")
                    WithdrawOption(savingsAccList);
                else if (option == "4")
                    DisplayDetails(savingsAccList);
                else if (option == "0")
                {
                    Console.WriteLine("---------\nGoodbye!\n-------- -");
                    break;
                }
                else
                    Console.WriteLine("Invalid option. Please try again.");

            }

            static void DisplayAll(List<SavingsAccount> sList)
            {
                foreach (SavingsAccount cc in sList)
                {
                    Console.WriteLine(cc.ToString());
                }

            }


            static void DepositOption(List<SavingsAccount> savingsAccList)
            {
                Console.Write("Enter Account Number: ");
                string findAcc = Console.ReadLine();
                SavingsAccount extracted = (SearchAcc(savingsAccList, findAcc));
                if (extracted == null)
                {
                    Console.WriteLine("Unable to find account number. Please try again.");
                }
                else
                {
                    Console.Write("Enter deposit amount: ");
                    double amount = Convert.ToDouble(Console.ReadLine());
                    extracted.Deposit(amount);
                    Console.WriteLine("$" + amount + " deposited successfully");
                    Console.WriteLine(extracted.ToString());
                }
            }

            static SavingsAccount SearchAcc(List<SavingsAccount> sList, string accNo)
            {
                foreach (SavingsAccount person in sList)
                {
                    if (person.AccNo == accNo)
                    {
                        return person;
                    }

                }
                return null;
            }

            static void WithdrawOption(List<SavingsAccount> savingsAccList)
            {
                Console.Write("Enter Account Number: ");
                string findAcc = Console.ReadLine();
                SavingsAccount extracted = (SearchAcc(savingsAccList, findAcc));
                if (extracted == null)
                {
                    Console.WriteLine("Unable to find account number. Please try again.");
                }
                else
                {
                    Console.Write("Amount to withdraw: ");
                    double amount = Convert.ToDouble(Console.ReadLine());

                    if (extracted.Withdraw(amount) == true)
                    {
                        Console.WriteLine("$" + amount + " withdrawn successfully");
                        Console.WriteLine(extracted.ToString());

                    }
                    else
                    {
                        Console.WriteLine("Insufficient funds.");
                    }


                }


            }
            static void DisplayDetails(List<SavingsAccount> savingsAccList)
            {
                Console.WriteLine("{0,-20}  {1,-20}  {2,-20}  {3,-20} {4,-20}", "Acc No", "Acc Name", "Balance", "Rate", "Interest Amt");
                foreach (SavingsAccount person in savingsAccList)
                {
                    Console.WriteLine("{0,-20}  {1,-20}  {2,-20}  {3,-20} {4,-20}",

                        person.AccNo, person.AccName, person.Balance, person.Rate, Convert.ToDouble(person.CalculateInterest(person.Rate)));
                }
            }
        }
    }
}
